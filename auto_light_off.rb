#!/usr/bin/ruby
IRMCLI='python /home/pi/irmcli/irmcli.py -p -f'
LIGHT_OFF='/home/pi/irmcli/light_off.json'

PING_TARGET='192.168.1.20'

COMMAND='ping '+PING_TARGET+' -c 2 >> /dev/null'
ret = system(COMMAND)
if( !ret ) then
`#{IRMCLI} #{LIGHT_OFF}`
end
