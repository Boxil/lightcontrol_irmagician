#!/bin/sh
IRMCLI='python /home/pi/irmcli/irmcli.py -p -f'
LIGHT_ON='/home/pi/irmcli/light_on.json'
PING_TARGET="192.168.1.20"

MAX_SECOND=18000
SLEEP_SEC=30

#loop until ping success
count=$MAX_SECOND
while [ $count -ne 0 ] ; do
    ping ${PING_TARGET} -c 2 >> /dev/null
    rc=$?
    if [ $rc -eq 0 ] ; then
	count=1 # break loop
    fi
    sleep $SLEEP_SEC
    count=`expr $count - $SLEEP_SEC`
done

if [ $rc -eq 0 ] ; then
    $IRMCLI $LIGHT_ON
fi
